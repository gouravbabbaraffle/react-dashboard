import React, { Component } from 'react';
import './App.css';
import Login from './login.js';
import {
NavLink 
} from 'react-router-dom';
class ChangePassword extends Component {
constructor(props) {
super(props);
this.state = {
email:'',valid: true,password:"",cpassword:""
}
this.validateEmail=this.validateEmail.bind(this);
this.handleEmailChange=this.handleEmailChange.bind(this);
}
validateEmail (email) {
const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
return re.test(email) ;
}
handleEmailChange(e) {
var n;
const email = e.target.value;
this.setState({
email: e.target.value
})
}
validateEmailBlur(e){
const email = e.target.value;
const emailvalid=this.validateEmail(email);
this.setState({
valid: emailvalid
})
}
handlePasswordChange(e){
const password = e.target.value;
this.setState({
password:e.target.value
})
}
handleCpasswordChange(e){
this.n=1;
const cpassword = e.target.value;
this.setState({
cpassword:e.target.value
})
}
onSubmitValidation()
{
if((this.state.email==="") || (this.state.password==="") || (this.state.cpassword===""))
{
alert('please fill all fields');
}
}
render() {
let fieldContainerClass = 'email-error';
let fieldContainer = 'pass-error';
let navigation='/'
const {email,valid,password,cpassword}  = this.state;
if(this.n===1){
if(password!==cpassword){
fieldContainer += ' error'
}
}
if((this.state.email!=="") && (this.state.password!=="") && (this.state.cpassword!==""))
{
navigation +='login'
}
if (!valid) {
fieldContainerClass += ' error'
}
return (
<div className="app flex-row align-items-center" id="change-pass-page">
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-md-6">
        <div className="card mx-4">
          <div className="card-block p-4">
            <h6>Create New Password
            </h6>        
            <div className={fieldContainerClass}>please enter valid email
            </div>
            <div className="input-group mb-3">
              <span className="input-group-addon">
                <i className="icon-lock">
                </i>
              </span>
              <input type="password" className="form-control" placeholder="Enter old Password"/>
            </div>
            <div className="input-group mb-3">
              <span className="input-group-addon">
                <i className="icon-lock">
                </i>
              </span>
              <input type="password" className="form-control" placeholder="New Password" value={this.state.password} onChange={this.handlePasswordChange.bind(this)}/>
            </div>
            <div className="input-group mb-4">
              <span className="input-group-addon">
                <i className="icon-lock">
                </i>
              </span>
              <input type="password" className="form-control" placeholder="Repeat password" value={this.state.cpassword} onChange={this.handleCpasswordChange.bind(this)}/>
            </div>
            <div className={fieldContainer}>password not match
            </div>
            <NavLink to="/dashborad">
              <button type="button" onClick={this.onSubmitValidation.bind(this)} className="btn btn-block btn-success">Save
              </button>
            </NavLink>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
);
}
}
export default ChangePassword;
