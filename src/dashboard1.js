import React, { Component } from 'react';
import {
Badge,
Row,
Col,
Progress,
Dropdown,
DropdownToggle,
DropdownMenu,
DropdownItem,
Card,
CardHeader,
CardBlock,
CardFooter,
CardTitle,
Button,
ButtonToolbar,
ButtonGroup,
ButtonDropdown,
Label,
Input,
Table
} from "reactstrap";
import firebaseApp from './Firebase.js';
class MainDashboard extends Component {
   constructor(props) {
    super(props);
 this.state = {
  

    };
    this.state.term="";
    this.state.usersList=[];
    this.state.rcount = 0;
     
  }
  componentDidMount(){
/* var x = document.getElementById("user-row").rows.length;
this.setState({
  rcount:x
})*/
const dbRefObject= firebaseApp.database().ref().child('usersList');

dbRefObject.orderByChild("UserId").startAt(105).on('value', snap =>{
    var address = [];
    
     snap.forEach((child) => {

      address.push({
          EmailId: child.val().EmailId,
          UserId: child.val().UserId,
          UserName: child.val().UserName,
          _key: child.key
        });

 this.setState({
        usersList:address,
        rcount:address.length

        
})
      });
     

});

  }
render() {
return (
<div className="dashboard-data">
  <Row>
    <Col xs="12" sm="6" lg="3">
      <Card className="text-white bg-primary">
        <CardBlock className="card-body pb-0">
          <p className="mb-0">Total Products
          </p>
          <h4>40
          </h4>
        </CardBlock>
      </Card>
    </Col>
    <Col xs="12" sm="6" lg="3">
      <Card className="text-white bg-info">
        <CardBlock className="card-body pb-0">
          <p className="mb-0">Total Users 
          </p>
          <h4>{this.state.rcount}
          </h4>
        </CardBlock>
      </Card>
    </Col>
    <Col xs="12" sm="6" lg="3">
      <Card className="text-white bg-warning">
        <CardBlock className="card-body pb-0">
          <p className="mb-0">Total Orders
          </p>
          <h4>243
          </h4>
        </CardBlock>
      </Card>
    </Col>
    <Col xs="12" sm="6" lg="3">
      <Card className="text-white bg-danger">
        <CardBlock className="card-body pb-0">
          <p className="mb-0">Total Earnings
          </p>
          <h4>$4807.00
          </h4>
        </CardBlock>
      </Card>
    </Col>
  </Row>
  <div className="animated fadeIn">
    <div className="row">
      <div className="col-lg-12">
        <div className="card">
          <div className="card-header">
            <i className="fa fa-align-justify">
            </i> User List
          </div>
          <div className="card-block">
            <table className="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th>User Id
                  </th>
                  <th>User Name
                  </th>
                  <th>Email Id
                  </th>
                </tr>
              </thead>
              <tbody id="user-row">
               {
                  this.state.usersList.map(userlist =>
                <tr key={userlist._key}>

                  <td>{userlist.UserId}</td>
                  <td>{userlist.UserName}</td>
                  <td>{userlist.EmailId}</td>
               
                </tr>
                 )}
               </tbody>
            </table>
            <nav>
              <ul className="pagination">
                <li className="page-item">
                  <a className="page-link" >Prev
                  </a>
                </li>
                <li className="page-item active">
                  <a className="page-link" >1
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" >2
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" >3
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" >4
                  </a>
                </li>
                <li className="page-item">
                  <a className="page-link" >Next
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
)
}
}
export default MainDashboard;
