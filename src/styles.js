
const React = require('react-native');

const { StyleSheet, Dimensions } = React;
const window = Dimensions.get('window');

module.exports = StyleSheet.create({

    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
    },

  headerView: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    height: 60,
    width: window.width,
  },

  menuButton: {
    position: 'absolute',
    left: 10,
    marginTop: 6,
    width: 25,
    height: 25,
  },

    Logo: {
        width: window.width * 0.4,
        height: window.width * 0.4,
        marginBottom: 20,
        resizeMode: 'contain'
    },

    Version: {
        marginTop: 5,
        marginBottom: 15,
        marginLeft: 20,
        marginRight: 20,
        fontSize: 15
    },

    text: {
        textAlign: 'center',
        fontSize: 15
    },

    Description: {
        marginTop: 30,
        marginLeft: 20,
        marginRight: 20,
        textAlign: 'center',
        fontSize: 15
    },

    buttonText: {
        margin: 5,
        fontSize: 20,
        textDecorationLine: 'underline',
        color: '#4990aa'
    }

});
