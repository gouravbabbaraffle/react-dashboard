import React, { Component } from 'react';
import './App.css';



class App extends Component {

  constructor(props) {
      super(props);
    
      this.state = {
        email:'',valid: true,password:"",cpassword:""
      
      }
      this.validateEmail=this.validateEmail.bind(this);
      this.handleEmailChange=this.handleEmailChange.bind(this);
   }

  
   validateEmail (email) {
   const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
   return re.test(email) ;
  }
  
  handleEmailChange(e) {
  var n;
      const email = e.target.value;
      const emailValid = this.validateEmail(email) ;

      this.setState({
        email: e.target.value,
        valid: emailValid
      })
  }
handlePasswordChange(e){
   const password = e.target.value;
    this.setState({
    password:e.target.value

    })
}
handleCpasswordChange(e){
   this.n=1;
   const cpassword = e.target.value;
    this.setState({
    cpassword:e.target.value

    })
}
  onSubmitValidation()
{
   if((this.state.email==="") || (this.state.password===""))
    {
        alert('please fill all fields');
    }
}
  render() {


    let fieldContainerClass = 'email-error';
    let fieldContainer = 'pass-error';
    

    const {email,valid,password,cpassword}  = this.state;
    if(this.n===1){
    if(password!==cpassword){
    fieldContainer += ' error'

    }
    }
    
    if (!valid) {
   fieldContainerClass += ' error'
    }


    return (
        <div className="app flex-row align-items-center" id="register-page">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-6">
              <div className="card mx-4">
                <div className="card-block p-4">
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  <div className="input-group mb-3">
                    <span className="input-group-addon"><i className="icon-user"></i></span>
                    <input type="text" className="form-control" placeholder="Username"/>
                  </div>
                  <div className="input-group mb-3">
                    <span className="input-group-addon">@</span>
                    <input type="text" className="form-control" placeholder="Email"  onChange={this.handleEmailChange} value={this.state.email} id="email"  name="email" autoComplete="off"/>
                  </div>
                  <div className={fieldContainerClass}>please enter valid email</div>
                  <div className="input-group mb-3">
                    <span className="input-group-addon"><i className="icon-lock"></i></span>
                    <input type="password" className="form-control" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange.bind(this)}/>
                  </div>
                  <div className="input-group mb-4">
                    <span className="input-group-addon"><i className="icon-lock"></i></span>
                    <input type="password" className="form-control" placeholder="Repeat password" value={this.state.cpassword} onChange={this.handleCpasswordChange.bind(this)}/>
                  </div>
                  <div className={fieldContainer}>password not match</div>
                  <button type="button" className="btn btn-block btn-success">Create Account</button>
                </div>
                
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
