
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import * as firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
    
    apiKey: "AIzaSyAms8kvIsb8lvg8bNdCyc6_CvimA7XCe_Q",
    authDomain: "cold-storage-829a5.firebaseapp.com",
    databaseURL: "https://cold-storage-829a5.firebaseio.com",
    projectId: "cold-storage-829a5",
    storageBucket: "cold-storage-829a5.appspot.com",
    messagingSenderId: "351646074694"
 
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
export default firebaseApp;
