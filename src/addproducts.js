import React, { Component } from 'react';
import {
    NavLink 
} from 'react-router-dom';

class AddProducts extends Component {
  render() {
    return (
      <div className="app add-products">
       <form className="form-horizontal">
  <div className="form-group">
    <label className="control-label col-sm-2" >id:</label>
    <div className="col-sm-10">
      <input type="text" className="form-control" placeholder="Enter id" />
    </div>
  </div>
  <div className="form-group">
    <label className="control-label col-sm-2">category:</label>
    <div className="col-sm-10"> 
      <input type="text" className="form-control"  placeholder="Enter category" />
    </div>
  </div>
  <div className="form-group">
    <label className="control-label col-sm-2" >price:</label>
    <div className="col-sm-10"> 
      <input type="text" className="form-control"  placeholder="Enter price" />
    </div>
  </div>
  <div className="form-group">
    <label className="control-label col-sm-2" >qty:</label>
    <div className="col-sm-10"> 
      <input type="text" className="form-control"  placeholder="Enter quantity" />
    </div>
  </div>
  <div className="form-group">
    <label className="control-label col-sm-2" >name:</label>
    <div className="col-sm-10"> 
      <input type="text" className="form-control"  placeholder="Enter name" />
    </div>
  </div>
  
  <div className="form-group"> 
    <div className="col-sm-offset-2 col-sm-10">
      <button type="submit" className="btn btn-default">Save</button>
       <NavLink to="/tabs" ><button type="submit" className="btn btn-default">cancel</button></NavLink>
 
    </div>
  </div>
</form>
      </div>
    );
  }
}

export default AddProducts;