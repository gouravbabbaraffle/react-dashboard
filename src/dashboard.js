import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'
import Header from './header.js';
import Sidebar from './sidebar.js';
import MainDashboard from './dashboard1.js';
import ManageProducts from './manageproducts.js';
import AddProduct from'./addproducts1.js';
import ManageOrder from'./manageorders1.js';
import ManageStore from'./managestore.js';
import OrderDetails from'./orderdetails.js';
import Breadcrumb from './Breadcrumb.js';
import AboutUs from './AboutUs.js';
import EditProduct from './editproduct.js';

class Dashboard extends Component {
render() {
return (
<div className="app">
  <Header/>
  <div className="app-body">
    <Sidebar {...this.props}/>
    <main className="main">
      <Breadcrumb />
      <div className="container-fluid">
        <Switch >
          <Route path="/dashboard" name="Dashboard" component={MainDashboard}/>
          <Route path="/manageproducts" name="Dashboard" component={ManageProducts}/>
          <Route path="/addproduct" name="Dashboard" component={AddProduct}/>
          <Route path="/aboutus" name="Dashboard" component={AboutUs}/>
          <Route path="/editproduct" name="Dashboard" component={EditProduct}/>
          <Route path="/manageorder" name="Dashboard" component={ManageOrder}/>
          <Route path="/managestore" name="Dashboard" component={ManageStore}/>
          <Route path="/orderdetails" name="Dashboard" component={OrderDetails}/>
          <Redirect from="/" to="/dashboard"/>
        </Switch>
      </div>
    </main>
  </div>
</div>
);
}
}
export default Dashboard;
