import React from 'react';
import {
    NavLink 
} from 'react-router-dom';
export default class Products extends React.Component {

  constructor(props) {
    super(props);
    //  this.state.products = [];
    this.state = {


    };
    this.state.term="";
    
    this.state.products = [
      {
        id: 1,
        category: 'Sporting Goods',
        price: '49.99',
        qty: 12,
        name: 'football'
      }, {
        id: 2,
        category: 'Sporting Goods',
        price: '9.99',
        qty: 15,
        name: 'baseball'
      }, {
        id: 3,
        category: 'Sporting Goods',
        price: '29.99',
        qty: 14,
        name: 'basketball'
      }, {
        id: 4,
        category: 'Electronics',
        price: '99.99',
        qty: 34,
        name: 'iPod Touch'
      }, {
        id: 5,
        category: 'Electronics',
        price: '399.99',
        qty: 12,
        name: 'iPhone 5'
      }, {
        id: 6,
        category: 'Electronics',
        price: '199.99',
        qty: 23,
        name: 'nexus 7'
      }
   ];
  }

  searchHandler(event)
{
this.setState(
  {term:event.target.value}
  )
}
onDelEvent(prod){
  var id = prod;
  var index = -1;
  console.log(id)
  // this.state.products.splice(id, 1);
  // this.setState(this.state.products);

  for(var i = 0 ; i<this.state.products.length; i++)
  {
       if(this.state.products[i].id === id)
       {
          index = i;
          this.state.products.splice(index, 1);
          this.setState(this.state.products);
       }
  }    
}

  render() {
    return (
      <div className="table-view">
      <div>
      <from>
      <input type="text" onChange={this.searchHandler.bind(this)} value={this.state.term} placeholder="Search..."/>
      </from>
      <div>
      <button type="button" data-toggle="modal" data-target="#myModal">Add category</button>
   <div id="myModal" className="modal fade" role="dialog">
  <div className="modal-dialog">


    <div className="modal-content">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal">&times;</button>
        <h4 className="modal-title">Add Products</h4>
      </div>
      <div className="modal-body">
         <div className="form-group">
            <label htmlFor="recipient-name" className="control-label">Category Name:</label>
            <input type="text" className="form-control" id="recipient-name"/>
          </div>
      </div>
      <div className="modal-footer">
        <NavLink to="/tabs"><button type="button" className="btn btn-default">Save</button></NavLink>

        <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>  

      <NavLink to="/addproducts"> <button>Add Products</button></NavLink>
</div>
      </div>
      <div className="tab-content" >

      <table className="table table-sm tab-pane fade in active" id="managetable">
            <tbody>
      {
        this.state.products.filter(searchingFor(this.state.term)).map(person =>
        

            
            <tr id={person.id} key={person.id}>
            <td className="col-md-2"><NavLink to="/order">{person.id}</NavLink></td>
            <td className="col-md-3">{person.category}</td>
            <td className="col-md-3">{person.price}</td>
            <td className="col-md-2">{person.qty}</td>
            <td className="col-md-3">{person.name}</td>
            <td className="col-md-2"><span onClick={this.onDelEvent.bind(this,person.id)} className="del-btn"><i className="fa fa-trash" aria-hidden="true"></i></span></td>

            </tr>
            )}
            </tbody>
            </table>
            <table id="secondpage" className="tab-pane fade">
            <tbody>
            <tr>
            <td>third</td>
            </tr>
            </tbody>
            </table>
            <table id="thirdpage" className="tab-pane fade">
            <tbody>
            <tr>
            <td>third</td>
            </tr>
            </tbody>
            </table>
            </div>
            <ul className="pagination pagination-lg nav nav-pills">
     <li className="active"><a data-toggle="pill" href="#managetable">1</a></li>
    <li><a data-toggle="pill" href="#secondpage">2</a></li>
    <li><a data-toggle="pill" href="#thirdpage">3</a></li>
    <li><a data-toggle="pill" href="#menu3">4</a></li>
    <li><a data-toggle="pill" href="#menu4">5</a></li>


  </ul>

      </div>

      
    );

  }
}
function searchingFor(term){
  return function(x){
  return x.name.toLowerCase().includes(term.toLowerCase());

  }
}