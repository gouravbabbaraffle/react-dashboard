import React, {Component} from "react";
import {
Link 
} from 'react-router-dom';
import {Badge, Row, Col,Input, TabContent, TabPane, Nav, NavItem,NavLink} from "reactstrap";
import classnames from "classnames";
import {Card,
CardHeader,
CardBlock,
Table,
Pagination,
PaginationItem,
PaginationLink
} from "reactstrap";
import DatePicker from 'react-datepicker';
import moment from 'moment';
import firebaseApp from './Firebase.js';
export default class ManageOrder extends Component {
constructor(props) {
super(props);
this.toggle = this.toggle.bind(this);
this.state = {
activeTab: '1',
orderRequests:[]
};
this.state.startDate=moment();
this.state.startDate1=moment();
this.state.term="";
}
toggle(tab) {
if (this.state.activeTab !== tab) {
this.setState({
activeTab: tab
});
}
}
handleDateChange(date) {
this.setState({
startDate: date
});
console.log(this.state.startDate)
}
handleDateChange1(date) {
this.setState({
startDate1: date
});
console.log(this.state.startDate1)
}
searchHandler(event)
{
this.setState({term:event.target.value})
}

componentDidMount(){
  const dbRefObject= firebaseApp.database().ref().child('orderRequest');

  dbRefObject.on('value', snap =>{
    var requests = [];
    
     snap.forEach((child) => {

      requests.push({
          orderId: child.val().orderId,
          orderNo: child.val().orderNo,
          UserName: child.val().UserName,
          orderItem: child.val().orderItem,
          totalPrice: child.val().totalPrice,
          orderTime: child.val().orderTime,
          pickupTime: child.val().pickupTime,
          _key: child.key
        });

 this.setState({
        orderRequests:requests,
               
})
      });
  });
}
render() {
return (
<div className="animated fadeIn manage-order">
  <div className="search-date">
    <div className="date-picker">
      <div>
        From:
        <DatePicker selected={this.state.startDate} onChange={this.handleDateChange.bind(this)} />
      </div>
      <div>
        To:
        <DatePicker selected={this.state.startDate1} onChange={this.handleDateChange1.bind(this)} />
      </div>
    </div>
  </div>
  <Row>
    <Col xs="12" md="12" className="mb-4">
      <Nav tabs>
        <NavItem>
          <NavLink
                   className={classnames({ active: this.state.activeTab === '1' })}
                   onClick={() => { this.toggle('1'); }}
            >
            Order Request
            <span className="request-number">(13)
            </span>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
                   className={classnames({ active: this.state.activeTab === '2' })}
                   onClick={() => { this.toggle('2'); }}
            >
            Completed Orders
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={this.state.activeTab}>
        <TabPane tabId="1">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify">
              </i> Order Request
            </CardHeader>
            <CardBlock className="card-body">
              <Table className="table-bordered table-striped" responsive>
                <thead>
                  <tr>
                    <th>Order Id
                    </th>
                    <th>Order No
                    </th>
                    <th>User Name
                    </th>
                    <th>Ordered Items
                    </th>
                    <th>Total Price
                    </th>
                    <th>Order Time
                    </th>
                    <th>Pick up Time
                    </th>
                    <th>Status
                    </th>
                  </tr>
                </thead>
                <tbody>{
                this.state.orderRequests.map(orders=>
                  <tr key={orders._key}>
                    <td>{orders.orderId}</td>
                    <td><Link to="/orderdetails">{orders.orderNo}</Link></td>
                    <td>{orders.UserName}</td>
                    <td>{orders.orderItem}</td>
                    <td>${orders.totalPrice}</td>
                    <td>{orders.orderTime}</td>
                    <td>{orders.pickupTime}</td>
                    <td>
                      <Input type="select">
                        <option value="0">Select
                        </option>
                        <option value="1">New Order
                        </option>
                        <option defaultValue value="2">Confirm Order
                        </option>
                        <option value="3">Order Ready for Pickup
                        </option>
                      </Input>
                    </td>
                  </tr>
                  )}
                  </tbody>
              </Table>
              <Pagination>
                <PaginationItem>
                  <PaginationLink previous href="#">pre
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem active>
                  <PaginationLink href="#">1
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">2
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">3
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">4
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink next href="#">next
                  </PaginationLink>
                </PaginationItem>
              </Pagination>
            </CardBlock>
          </Card>
        </TabPane>
        <TabPane tabId="2">
          <Card>
            <CardHeader>
              <i className="fa fa-align-justify">
              </i> Completed Order
            </CardHeader>
            <CardBlock className="card-body">
              <Table className="table-bordered table-striped" responsive>
                <thead>
                  <tr>
                    <th>Order Id
                    </th>
                    <th>Order No
                    </th>
                    <th>User Name
                    </th>
                    <th>Ordered Items
                    </th>
                    <th>Total Price
                    </th>
                    <th>Order Time
                    </th>
                    <th>Pick up Time
                    </th>
                    <th>Status
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>45
                    </td>
                    <td>23
                    </td>
                    <td>Gourav babbar
                    </td>
                    <td>Green salad
                    </td>
                    <td>$18
                    </td>
                    <td>
                      <span className="order-date">04/07/2017
                      </span>-
                      <span className="order-time">09:56
                      </span>
                    </td>
                    <td>
                      <span className="order-date">04/07/2017
                      </span>-
                      <span className="order-time">09:56
                      </span>
                    </td>
                    <td>
                      <Badge className="order-status" color="danger">Completed
                      </Badge>
                    </td>
                  </tr>
                  <tr>
                    <td>45
                    </td>
                    <td>23
                    </td>
                    <td>Gourav babbar
                    </td>
                    <td>Green salad
                    </td>
                    <td>$18
                    </td>
                    <td>
                      <span className="order-date">04/07/2017
                      </span>-
                      <span className="order-time">09:56
                      </span>
                    </td>
                    <td>
                      <span className="order-date">04/07/2017
                      </span>-
                      <span className="order-time">09:56
                      </span>
                    </td>
                    <td>
                     <Badge className="order-status" color="danger">Completed
                      </Badge>
                    </td>
                  </tr>
                  <tr>
                    <td>45
                    </td>
                    <td>23
                    </td>
                    <td>Gourav babbar
                    </td>
                    <td>Green salad
                    </td>
                    <td>$18
                    </td>
                    <td>
                      <span className="order-date">04/07/2017
                      </span>-
                      <span className="order-time">09:56
                      </span>
                    </td>
                    <td>
                      <span className="order-date">04/07/2017
                      </span>-
                      <span className="order-time">09:56
                      </span>
                    </td>
                    <td>
                      <Badge className="order-status" color="danger">Completed
                      </Badge>
                    </td>
                  </tr>
                </tbody>
              </Table>
              <Pagination>
                <PaginationItem>
                  <PaginationLink previous href="#">pre
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem active>
                  <PaginationLink href="#">1
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">2
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">3
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink href="#">4
                  </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                  <PaginationLink next href="#">next
                  </PaginationLink>
                </PaginationItem>
              </Pagination>
            </CardBlock>
          </Card>
        </TabPane>
      </TabContent>
    </Col>
  </Row>
</div>)}}
function searchingFor(term){
return function(x){
return x.name.toLowerCase().includes(term.toLowerCase());
}
}
