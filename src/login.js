import React, { Component } from 'react';
import './App.css';
import {
NavLink 
} from 'react-router-dom';
import firebaseApp from './Firebase.js';
import * as firebase from 'firebase';
class Login extends Component {
constructor(props) {
super(props);
this.state = {
email:'',valid: true,password:""
}
this.validateEmail=this.validateEmail.bind(this);
this.handleEmailChange=this.handleEmailChange.bind(this);
}
validateEmail (email) {
const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ ;
return re.test(email) ;
}
handleEmailChange(e) {
const email = e.target.value;
this.setState({
email: e.target.value
})
}
validateEmailBlur(e){
const email = e.target.value;
const emailvalid=this.validateEmail(email);
this.setState({
valid: emailvalid
})
}
handlePasswordChange(e){
const password = e.target.value;
this.setState({
password:e.target.value
})
}
onSubmitValidation()
{
if((this.state.email==="") || (this.state.password===""))
{
alert('please fill all fields');
}
}
onclick(){
this.onSubmitValidation();
this.signinauth();
}

signinauth(e){

  const email1=this.state.email;
  const pass2=this.state.password;

  const promise= firebaseApp.auth().signInWithEmailAndPassword(email1,pass2);
  promise
  .then(user => console.log("welcome"))
  .catch(e => console.log(e.message));

  firebaseApp.auth().onAuthStateChanged(firebaseUser =>{

if (firebaseUser) {
  console.log("welcome",firebaseUser.email)
    window.location = "/dashboard";
}else
console.log('not logged in')

  });

}
 google_login_in(){
        var provider = new firebase.auth.GoogleAuthProvider(); 
        provider.addScope('https://www.googleapis.com/auth/plus.login');
        firebaseApp.auth().signInWithPopup(provider).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
        }).catch(function(error) {
            var errorCode     = error.code;
            var errorMessage  = error.message;
            var email         = error.email;
            var credential    = error.credential;
        });  

          firebaseApp.auth().onAuthStateChanged(firebaseUser =>{

if (firebaseUser) {
  console.log("welcome",firebaseUser.email)
  window.location = "/dashboard";
}else
console.log('not logged in')

  });


    }
fb_login_in(){
        var provider = new firebase.auth.FacebookAuthProvider(); 
            firebaseApp.auth().signInWithPopup(provider).then(function(result) {
              //console.log(result)
            // var token = result.credential.accessToken;
            // var user = result.user;
            // console.log(token);
            // console.log(user);

        }).catch(function(error) {
            var errorCode     = error.code;
            var errorMessage  = error.message;
            var email         = error.email;
            var credential    = error.credential;
            console.log(errorCode);
            console.log(errorMessage);
            console.log(email);
            console.log(credential);
        });  

          firebaseApp.auth().onAuthStateChanged(firebaseUser =>{
            //console.log("welcome",firebaseUser)
       

if (firebaseUser) {
 // console.log("welcome",firebaseUser)
 
 window.location = "/dashboard";
}else
console.log('not logged in')

  });


    }

render() {
let fieldContainerClass = 'email-error';
const {email,valid}  = this.state;
if (!valid) {
fieldContainerClass += ' error'
}
return (
<div className="app flex-row align-items-center" id="login-page">
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-md-8">
        <div className="card-group mb-0">
          <div className="card p-4">
            <div className="card-block">
              <h1>Login
              </h1>
              <p className="text-muted">Sign In to your account
              </p>
              <div className="input-group mb-3">
                <span className="input-group-addon">
                  <i className="icon-user">
                  </i>
                </span>
                <input type="text" className="form-control" placeholder="Enter Email" onBlur={this.validateEmailBlur.bind(this)} onChange={this.handleEmailChange.bind(this)}  value={this.state.email} id="email"  name="email" autoComplete="off"/>
              </div>
              <div className={fieldContainerClass}>please enter valid email
              </div>
              <div className="input-group mb-4">
                <span className="input-group-addon">
                  <i className="icon-lock">
                  </i>
                </span>
                <input type="password" className="form-control" placeholder="Password" id="pwd" onChange={this.handlePasswordChange.bind(this)} name="pwd"/>
              </div>
              <div className="checkbox">
                <label>
                  <input type="checkbox" name="remember"/> Remember me
                </label>
              </div>
              <div className="row margin-0">
                <div className="col-6">
                
                  <button type="button" className="btn btn-primary px-4" onClick={this.onclick.bind(this)}>Login
                  </button>
                  
                </div>
                <div className="col-6">
                
                  <button type="button" className="btn btn-primary px-4" onClick={this.google_login_in.bind(this)}>Login with google
                  </button>
                  
                </div>
                <div className="col-6">
                
                  <button type="button" className="btn btn-primary px-4" onClick={this.fb_login_in.bind(this)}>Login with fb
                  </button>
                  
                </div>
                <div className="col-6 text-right">
                  <NavLink to="/forgot">
                    <button type="button" className="btn btn-link px-0">Forgot password?
                    </button>
                  </NavLink>
                </div>
              </div>
            </div>
          </div>
          <div className="card card-inverse card-primary py-5 d-md-down-none" style={{ width: 44 + '%' }}>
            <div className="card-block text-center margin-top-40">
              <div>
                <h2>Sign up
                </h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </p>
                <NavLink to="/register">
                  <button type="button" className="btn btn-primary active mt-3">Register Now
                  </button>
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
);
}
}
export default Login;
