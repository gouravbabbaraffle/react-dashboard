import React, { Component } from 'react';
import {
NavLink 
} from 'react-router-dom';
class OrderDetails extends Component {
constructor(props) {
super(props);
this.state = {
};
this.state.term="";
this.state.products = [
{
id: 101,
itemCode: 1025,
Name: 'Green salad',
category: 'Salad',
size: '2.5L',
price: '12',
qty:'2'
}, {
id: 102,
itemCode: 1026,
Name: 'Lemon Tea',
category: 'beverages',
size: '300 ML',
price: '15',
qty:'1'
}
];
}
render() {
return (
<div className="order-details">
  <div className="animated fadeIn">
    <div className="row">
      <div className="col-lg-12">
        <div className="card">
          <div className="card-block">
            <div className="order-data">
              <div className="order-timings">
                <span>Date:
                  <span className="order-date">05/july/2017
                  </span>
                </span>
                <span>Order No:
                  <span className="order-no">45
                  </span>
                </span>
                <span>Schedule Pickup:
                  <span className="order-schedule">12:45
                  </span>
                </span>
              </div>
              <div className="det"> 
              <div className="order-stat">
                <span className="order-received">Received
                </span>
                <span className="order-confirmed">Confirmed
                </span>
                <span className="order-prepared">Prepared
                </span>
                <span className="order-pickup">PickUp
                </span>
              </div>
              <div className="order-stat-time">
                <span className="order-received-time" >10:45
                </span>
                <span className="order-confirmed-time">10:50
                </span>
                <span className="order-prepared-time">11:30
                </span>
                <span className="order-pickup-time">11:45
                </span>
              </div>
              </div>
            </div>
          </div>
        </div>
        <div className="card">
          <div className="card-header">
            <i className="fa fa-align-justify">
            </i>Product Details
          </div>
          <div className="card-block">
            <table className="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th>Product Id
                  </th>
                  <th>Item Code
                  </th>
                  <th>Name
                  </th>
                  <th>category
                  </th>
                  <th>Size
                  </th>
                  <th>qty
                  </th>
                  <th>Price(Each)
                  </th>
                  <th>Total Price
                  </th>
                </tr>
              </thead>
              <tbody>
                {
                this.state.products.map(person =>
                <tr id={person.id} key={person.id}>
                  <td>{person.id}
                  </td>
                  <td>{person.itemCode}
                  </td>
                  <td>{person.Name}
                  </td>
                  <td>{person.category}
                  </td>
                  <td>{person.size}
                  </td>
                  <td>{person.qty}
                  </td>
                  <td>${person.price}
                  </td>
                  <td>${person.price*person.qty}
                  </td>
                </tr> 
                )}   
                <tr>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>Sub Total
                  </td>
                  <td> 
                  </td>
                </tr>  
                <tr>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>GST
                  </td>
                  <td> 
                  </td>
                </tr> 
                <tr>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>
                  </td>
                  <td>Total
                  </td>
                  <td> 
                  </td>
                </tr>                               
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
)
}
}
export default OrderDetails;
