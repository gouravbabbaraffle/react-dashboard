import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Login from './login.js';
import Register from './register.js';
import Forgot from './forgot.js';
import Sidebar from './sidebar.js';
import Header from './header.js';
import Dashboard from './dashboard';
import Tables from './table.js';
import ChangePassword from './changepass.js'
import registerServiceWorker from './registerServiceWorker';

import {
BrowserRouter as Router,
Route,Switch,HashRouter
} from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
const history = createBrowserHistory()
ReactDOM.render((
<Router history={history}>
  <Switch>
    <Route exact path="/login" name="Login Page" component={Login}/>
    <Route exact path="/register" name="Register Page" component={Register}/>
    <Route path="/forgot" name="Home" component={Forgot}/>
    <Route path="/changepassword" name="Home" component={ChangePassword}/>
     <Route path="/" name="Home" component={Dashboard}/>
  </Switch>
</Router>), document.getElementById('root'));
