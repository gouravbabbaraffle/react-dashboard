import React, { Component } from 'react';
import { Dropdown, DropdownMenu, DropdownItem } from 'reactstrap';
import {
NavLink 
} from 'react-router-dom';
import {browserHistory} from 'react-router'
import firebaseApp from './Firebase.js';
import Login from './login.js';
import * as firebase from 'firebase';
class Header extends Component {
constructor(props) {
super(props);
this.toggle = this.toggle.bind(this);
this.state = {
dropdownOpen: false,
userName:null,
url:null
};
}
toggle() {
this.setState({
dropdownOpen: !this.state.dropdownOpen
});
}
sidebarToggle(e) {
e.preventDefault();
document.body.classList.toggle('sidebar-hidden');
}
sidebarMinimize(e) {
e.preventDefault();
document.body.classList.toggle('sidebar-minimized');
}
mobileSidebarToggle(e) {
e.preventDefault();
document.body.classList.toggle('sidebar-mobile-show');
}
asideToggle(e) {
e.preventDefault();
document.body.classList.toggle('aside-menu-hidden');
}
logout(){
  firebaseApp.auth().signOut();
  window.location="/login";
}

componentDidMount(){
   const self=this;
  firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
     var displayName = user.displayName;
        var email = user.email;
        var emailVerified = user.emailVerified;
        var photoURL = user.photoURL;
        var uid = user.uid;
        var providerData = user.providerData;
    self.setState({userName:displayName});
    self.setState({url:photoURL});
   // console.log(self.state.url)
  } else {
     console.log("not signed in")
  }
});
}
render() {

return (
<header className="app-header navbar">
  <button className="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button" onClick={this.mobileSidebarToggle}>&#9776;
  </button>
  <a className="navbar-brand">
  </a>
  <ul className="nav navbar-nav d-md-down-none">
    <li className="nav-item">
      <button className="nav-link navbar-toggler sidebar-toggler" type="button" onClick={this.sidebarToggle}>&#9776;
      </button>
    </li>
    <li className="nav-item px-3">
      <NavLink to="/dashboard">Dashboard
      </NavLink>
    </li>
  </ul>
  <ul className="nav navbar-nav ml-auto">
    <li className="nav-item">
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <button onClick={this.toggle} className="nav-link dropdown-toggle" data-toggle="dropdown" type="button" aria-haspopup="true" aria-expanded={this.state.dropdownOpen}>
          <img src={this.state.url} className="img-avatar" alt="admin@bootstrapmaster.com"/>
          <span className="d-md-down-none">{this.state.userName}
          </span>
        </button>
        <DropdownMenu className="dropdown-menu-right">
          <DropdownItem>
            <i className="fa fa-user">
            </i> Profile
          </DropdownItem>
          <DropdownItem>
            <i className="fa fa-wrench">
            </i>
            <NavLink to="/changepassword">Change Password
            </NavLink> 
          </DropdownItem>
          <DropdownItem data-toggle="modal" data-target="#myModal1">
            <i className="fa fa-lock">
            </i> Logout
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </li>
    <li className="nav-item d-md-down-none">
      <button className="nav-link navbar-toggler aside-menu-toggler" type="button" onClick={this.asideToggle}>&#9776;
      </button>
    </li>
  </ul>
  <div id="myModal1" className="modal fade" role="dialog">
    <div className="modal-dialog">
      <div className="modal-content">
        <div className="modal-body">
          <h4>Do you really want to exit
          </h4>
        </div>
        <div className="modal-footer">
          
            <button type="button" onClick={this.logout.bind(this)} className="btn btn-default">yes
            </button>
          
          <button type="button" className="btn btn-default" data-dismiss="modal">cancel
          </button>
        </div>
      </div>
    </div>
  </div>
</header>
)
}
}
export default Header;