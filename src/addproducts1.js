import React from 'react';
import {
  Badge,
  Row,
  Col,
  Progress,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardBlock,
  CardFooter,
  CardTitle,
  Button,
  ButtonToolbar,
  ButtonGroup,
  ButtonDropdown,
  Label,
  Input,
  Table,FormGroup
} from "reactstrap";
import {
    NavLink 
} from 'react-router-dom';
import firebaseApp from './Firebase.js';

export default class AddProduct extends React.Component{
  constructor(props) {
    super(props);
 this.state = {
      Productid:"",
      Itemcode:"",
      pName:"",
      price:"",
      size:"",
      category:"",
      Category:[]
       };
  }
componentDidMount(){



const dbRefObject1= firebaseApp.database().ref().child('productCategory').child('category');



     dbRefObject1.on('value', snap =>{
    var categories = [];
   
     snap.forEach((child) => {

      categories.push({
          category: child.val(),
          _key:child.val()
        });
     this.setState({
        Category:categories
                
})
     

});

  });
   }

  Productidinfo(e){
const Productid = e.target.value;
this.setState({
Productid:e.target.value
})
console.log(Productid)
}
  Productnameinfo(e){
const pName = e.target.value;
this.setState({
pName:e.target.value
})
console.log(pName)
}
  Productcodeinfo(e){
const Itemcode = e.target.value;
this.setState({
Itemcode:e.target.value
})
console.log(Itemcode)
}
  Productsizeinfo(e){
const size = e.target.value;
this.setState({
size:e.target.value
})
console.log(size)
}
  Productpriceinfo(e){
const price = e.target.value;
this.setState({
price:e.target.value
})
console.log(price)
}
Productcategoryinfo(e){
  const category = e.target.value;
this.setState({
category:e.target.value
})
console.log(category)
}
  Addproductdata(){
     var dbRefObject= firebaseApp.database().ref().child('productList');


dbRefObject.push({Productid: this.state.Productid,
          Itemcode: this.state.Itemcode,
          pName: this.state.pName,
          category: this.state.category,
          size: this.state.size,
          price: this.state.price});
  }
	render(){
		return( 
			<div className="add-product">
			<Col xs="12">
            <Card>
              <CardHeader>
                <strong>Add Products</strong>
                
              </CardHeader>
              <CardBlock className="card-body">
              <FormGroup>
                  <Label htmlFor="vat">Product Id</Label>
                  <Input type="text" id="vat" placeholder="Enter Product Id" value={this.state.Productid} onChange={this.Productidinfo.bind(this)}/>
                </FormGroup>
              <FormGroup>
                  <Label htmlFor="vat">Item Code</Label>
                  <Input type="text" id="vat" placeholder="Enter Item Code" value={this.state.Itemcode} onChange={this.Productcodeinfo.bind(this)}/>
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="company">Product Name</Label>
                  <Input type="text" id="company" placeholder="Enter Product Name" value={this.state.pName} onChange={this.Productnameinfo.bind(this)}/>
                </FormGroup>
                 <FormGroup>
                      <Label htmlFor="ccmonth">Category</Label>
                      <select className="form-control" value={this.state.category} onChange={this.Productcategoryinfo.bind(this)}>
           <option value=""> Select Category</option>
            {
            this.state.Category.map(pcategory =>
           
           <option value={pcategory.category} key={pcategory._key}>{pcategory.category}</option>
           ) }
           </select>
                    </FormGroup>
                
                <FormGroup>
                  <Label htmlFor="street">Product Size</Label>
                  <Input type="text" id="street" placeholder="Enter Product Size" value={this.state.size} onChange={this.Productsizeinfo.bind(this)}/>
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="street">Price(Each)</Label>
                  <Input type="text" id="street" placeholder="Enter Price" value={this.state.price} onChange={this.Productpriceinfo.bind(this)}/>
                </FormGroup>

                
              </CardBlock>
            </Card>
          </Col>
          <div className="category-products-btn">
        <button type="button" className="btn btn-secondary" onClick={this.Addproductdata.bind(this)}>Save</button>
       <NavLink to="/manageproducts"><button type="button" className="btn btn-secondary">Cancel</button></NavLink>
        </div>
        </div>
                  )
	}
}