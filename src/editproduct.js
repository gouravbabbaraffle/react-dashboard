import React from 'react';
import {
  Badge,
  Row,
  Col,
  Progress,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Card,
  CardHeader,
  CardBlock,
  CardFooter,
  CardTitle,
  Button,
  ButtonToolbar,
  ButtonGroup,
  ButtonDropdown,
  Label,
  Input,
  Table,FormGroup
} from "reactstrap";
import {
    NavLink 
} from 'react-router-dom';
import ManageProducts from './manageproducts.js';
import firebaseApp from './Firebase.js';
import createBrowserHistory from 'history/createBrowserHistory';

export default class EditProduct extends React.Component{
  constructor(props) {
    super(props);
     this.state = {

      Productid:props.location.data.person.Productid,
       isMounted: false
    };
  

}

productidchange(e){
  
const Productid=e.target.value;
if (this.state.isMounted) {
  this.setState({
Productid:e.target.value
  });}
}

updateproductlist(){

   var dbRefObject= firebaseApp.database().ref().child('productList').child(this.props.location.data.person._key);
dbRefObject.update({Productid:this.state.Productid});
//this.context.router.transitionTo("/manageproducts");
//createBrowserHistory.push('/manageproducts');
window.location = "/manageproducts";
//console.log(this.props.location.data.person._key)
}

/*itemchange(e){
const Itemcode=e.target.value;
  this.setState({
Itemcode:e.target.value
  })
}
productnamechange(e){
const Itemcode=e.target.value;
  this.setState({
pName:e.target.value
  })
}
productpricechange(e){
const price=e.target.value;
  this.setState({
price:e.target.value
  })
}
productsizechange(e){
const size=e.target.value;
  this.setState({
size:e.target.value
  })
}*/

componentDidMount(){
   this.setState({isMounted: true})
}
	render(){
    console.log(this.props.location.data.person.Productid)
		return( 
			<div className="add-product">
			<Col xs="12">
            <Card>
              <CardHeader>
                <strong>Edit Product </strong>
                
              </CardHeader>
              <CardBlock className="card-body">
              <FormGroup>
                  <Label htmlFor="vat">Product Id</Label>
                <Input type="text" id="vat" defaultValue={this.props.location.data.person.Productid} onChange={this.productidchange.bind(this)} />
                </FormGroup>
              <FormGroup>
                  <Label htmlFor="vat">Item Code</Label>
                  <Input type="text" id="vat" defaultValue={this.props.location.data.person.Itemcode}  />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="company">Product Name</Label>
                  <Input type="text" id="company" defaultValue={this.props.location.data.person.pName} />
                </FormGroup>
                 <FormGroup>
                      <Label htmlFor="ccmonth">Category</Label>
                      <Input type="text" defaultValue={this.props.location.data.person.category} />
                 </FormGroup>
                
                <FormGroup>
                  <Label htmlFor="street">Product Size</Label>
                  <Input type="text" id="street" defaultValue={this.props.location.data.person.size} />
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="street">Price(Each)</Label>
                  <Input type="text" id="street" defaultValue={this.props.location.data.person.price} />
                </FormGroup>

                
              </CardBlock>
            </Card>
          </Col>
          <div className="category-products-btn">
        <button type="button" className="btn btn-secondary" onClick={this.updateproductlist.bind(this)}>Save</button>
       <NavLink to="/manageproducts"><button type="button" className="btn btn-secondary">Cancel</button></NavLink>
        </div>
        </div>
                  )
	}
}

