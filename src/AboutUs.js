/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  ScrollView
} from 'react-native';
import styles from './styles.js';
import { assets } from './Assets/Assets/Assets.js';

const buttons = ['Terms of Use', 'Privacy Policy', 'Third Party Notices']

class AboutUs extends Component {

  // Render View
  render() {
      return (
            <View style={styles.container}>
                                <ScrollView contentContainerStyle={{flex: 1}, {justifyContent: 'center'}, {alignItems: 'center'}}>
                    <Image source={assets.defaultProfilePic} style={styles.Logo} />
                    <Text style={styles.text}>React Native POC™</Text>
                    <Text style={styles.Version}>Version 1.2.1</Text>
                    <Text style={styles.text}>Copyright 2003-2016 React native and/or Facebook</Text>
                    <Text style={styles.Description}> React Native lets you build mobile apps using only JavaScript. It uses the same design as React, letting you compose a rich mobile UI from declarative components.</Text>
                    <View style={{flex:1}, {height: 20}} />
                    {this.renderButtons()}
                </ScrollView>
            </View>
      );
  }

  //components
  

  renderButtons() {
      return buttons.map((element, index) => {
          return (
             
                  <Text style={styles.buttonText}> {element} </Text>
              
          )
      });

  }
  // Button Actions
  buttonPressed(index) {
    console.log("\n\nAbout Us -> Button Pressed at index " + index);
  }

}

export default AboutUs
