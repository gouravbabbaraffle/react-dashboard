const routes = {
  '/': 'Dashboard',
  '/dashboard': '',
  '/manageorder': 'ManageOrder',
  '/managestore': 'ManageStore',
  '/manageproducts':'ManageProducts',
  '/addproduct':'ManageProducts / AddProduct',
  '/editproduct':'ManageProducts / EditProduct',

  '/orderdetails':'OrderDetails'
};
export default routes;