export const assets = {
    "defaultProfilePic" : require('./Images/defaultProfilePic.png'),
    "sidemenu"      : require('./Images/menu.png'),
    "search"        : require('./Images/search.png'),
    "nonveg"        : require('./Images/nonveg.png'),
    "veg"           : require('./Images/veg.png'),
    "backButton"    : require('./Images/backArrow.png'),
    "loginLogo"     : require('./Images/loginLogo.png'),
    "checkedNormal" : require('./Images/checked_normal.png'),
    "checkedSelected": require('./Images/checked_selected.png'),
    "add"           : require('./Images/add.png'),
    "noResult"      : require('./Images/no-results.png'),
    "order"         : {
        "summary"  : require('./Images/Order/orderSummary.png'),
        "status"   : {
            "placed"        : require('./Images/Order/placed.png'),
            "packed"        : require('./Images/Order/packed.png'),
            "dispatched"    : require('./Images/Order/dispatched.png'),
            "delivered"     : require('./Images/Order/delivered.png'),
        },
    },
    "sideMenu"  : {
        "todaysMenu"    : require('./Images/SideMenu/todaysMenu.png'),
        "myAddresses"   : require('./Images/SideMenu/mapPin.png'),
        "orderHistory"  : require('./Images/SideMenu/orderHistory.png'),
        "aboutUs"       : require('./Images/SideMenu/aboutUs.png')
    },
    "dummy"     : {
        "productImage"  : require('./Images/dummy/productImage.jpg')
    },

  }
