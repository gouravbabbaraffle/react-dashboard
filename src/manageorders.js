import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import moment from 'moment'
class Manageorder extends Component {
constructor(props) {
super(props);
//  this.state.products = [];
this.state = {
};
this.state.startDate=moment();
this.state.startDate1=moment();
this.state.startDate2=moment();
this.state.startDate3=moment();
this.state.term="";
this.state.products = [
{
id: 1,
category: 'Sporting Goods',
price: '49.99',
qty: 12,
name: 'football'
}, {
id: 2,
category: 'Sporting Goods',
price: '9.99',
qty: 15,
name: 'baseball'
}, {
id: 3,
category: 'Sporting Goods',
price: '29.99',
qty: 14,
name: 'basketball'
}, {
id: 4,
category: 'Electronics',
price: '99.99',
qty: 34,
name: 'iPod Touch'
}, {
id: 5,
category: 'Electronics',
price: '399.99',
qty: 12,
name: 'iPhone 5'
}, {
id: 6,
category: 'Electronics',
price: '199.99',
qty: 23,
name: 'nexus 7'
}
];
}
handleDateChange(date) {
this.setState({
startDate: date
});
}
handleDateChange1(date) {
this.setState({
startDate: date
});
}
handleDateChange2(date) {
this.setState({
startDate: date
});
}
handleDateChange3(date) {
this.setState({
startDate: date
});
}
searchHandler(event)
{
this.setState({term:event.target.value})
}
render() {
return (
<div className="manage-order">
  <ul className="nav nav-pills">
    <li className="active">
      <a data-toggle="pill" href="#home">Order Requests
      </a>
    </li>
    <li>
      <a data-toggle="pill" href="#menu1">Completed Orders
      </a>
    </li>
  </ul>
  <div className="tab-content">
    <div id="home" className="tab-pane fade in active">
      <div className="table-view">
        <div>
          <from>
            <input type="text" onChange={this.searchHandler.bind(this)} value={this.state.term} placeholder="Search..."/>
          </from>
          <div className="date-picker">
            <div>
              From:
              <DatePicker selected={this.state.startDate} onChange={this.handleDateChange.bind(this)} />
            </div>
            <div>
              To:
              <DatePicker selected={this.state.startDate1} onChange={this.handleDateChange1.bind(this)} />
            </div>
          </div>
          <div>
          </div>
        </div>
        <table className="table table-sm">
          <tbody>
            {
            this.state.products.filter(searchingFor(this.state.term)).map(person =>
            <tr id={person.id} key={person.id}>
              <td className="col-md-3">{person.category}
              </td>
              <td className="col-md-3">{person.price}
              </td>
              <td className="col-md-3">{person.qty}
              </td>
              <td className="col-md-3">{person.name}
              </td>
              <td className="col-md-3">
                <select>
                  <option>Select
                  </option>
                  <option>New Order
                  </option>
                  <option>confirm Order
                  </option>
                  <option>Order Ready for pickup
                  </option>
                </select>
              </td>
            </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
    <div id="menu1" className="tab-pane fade">
      <div className="table-view">
        <div>
          <from>
            <input type="text" onChange={this.searchHandler.bind(this)} value={this.state.term} placeholder="Search..."/>
          </from>
          <div className="date-picker">
            <div>
              From:
              <DatePicker selected={this.state.startDate2} onChange={this.handleDateChange2.bind(this)} />
            </div>
            <div>
              To:
              <DatePicker selected={this.state.startDate3} onChange={this.handleDateChange3.bind(this)} />
            </div>
          </div>
          <div>
          </div>
        </div>
        <table className="table table-sm">
          <tbody>
            {
            this.state.products.filter(searchingFor(this.state.term)).map(person =>
            <tr id={person.id} key={person.id}>
              <td className="col-md-3">{person.category}
              </td>
              <td className="col-md-3">{person.price}
              </td>
              <td className="col-md-3">{person.qty}
              </td>
              <td className="col-md-3">{person.name}
              </td>
              <td className="col-md-3">Confirmed
              </td>
            </tr>
            )}
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
)}}
export default Manageorder;
function searchingFor(term){
return function(x){
return x.name.toLowerCase().includes(term.toLowerCase());
}
}
