import React from 'react';
import {
Badge,
Row,
Col,
Progress,
Dropdown,
DropdownToggle,
DropdownMenu,
DropdownItem,
Card,
CardHeader,
CardBlock,
CardFooter,
CardTitle,
Button,
ButtonToolbar,
ButtonGroup,
ButtonDropdown,
Label,
Input,
Table,
FormGroup
} from "reactstrap";
import {
NavLink 
} from 'react-router-dom';
export default class ManageStore extends React.Component{
render(){
return( 
<div className="manage-store">
  <Col xs="12">
    <Card>
      <CardHeader>
        <strong>Account
        </strong>
      </CardHeader>
      <CardBlock className="card-body">
        <Row xs="12">
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="vat">Name
              </Label>
              <Input type="text" id="vat" />
            </FormGroup>
          </Col>
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="vat">Mobile No.
              </Label>
              <Input type="text" id="vat" />
            </FormGroup>
          </Col>
        </Row>
        <Row>
        <Col xs="6">
          <FormGroup>
            <Label htmlFor="company">Email
            </Label>
            <Input type="text" id="company" />
          </FormGroup>
        </Col>
        </Row>
      </CardBlock>
      <CardHeader>
        <strong>Store Information
        </strong>
      </CardHeader>
      <CardBlock className="card-body">
        <Row xs="12">
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="vat">Store Name
              </Label>
              <Input type="text" id="vat" />
            </FormGroup>
          </Col>
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="vat">Contact No.
              </Label>
              <Input type="text" id="vat" />
            </FormGroup>
          </Col>
          <Col xs="12">
            <FormGroup className="form-inline">
              <Label htmlFor="company">Store Address
              </Label>
              <div className="col-sm-1">
              </div>
              <Input type="text" id="company" />
              <div className="col-sm-1">
              </div>
              <Input type="text" id="company" />
            </FormGroup>
          </Col>
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="vat">City
              </Label>
              <Input type="text" id="vat" />
            </FormGroup>
          </Col>
          <Col xs="6">
            <FormGroup>
              <Label htmlFor="vat">Country
              </Label>
              <Input type="text" id="vat" />
            </FormGroup>
          </Col>
        </Row>
      </CardBlock>
    </Card>
  </Col>
  <div className="category-products-btn">
    <button type="button" className="btn btn-secondary">Save
    </button>
    <NavLink to="/manageproducts">
      <button type="button" className="btn btn-secondary">Cancel
      </button>
    </NavLink>
  </div>
</div>
)}}
