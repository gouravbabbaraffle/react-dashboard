import React, { Component } from 'react';
import './App.css';
class Forgot extends Component {
constructor(props) {
super(props);
this.state = {
email: '',
valid: true,
};
this.handleEmailChange = this.handleEmailChange.bind(this);
}
validateEmail (email) {
const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
return re.test(email)
}
handleEmailChange(e) {
const email = e.target.value;
this.setState({
email: e.target.value
})
}
validateEmailBlur(e){
const email = e.target.value;
const emailvalid=this.validateEmail(email);
this.setState({
valid: emailvalid})
}
onSubmit()
{
if((this.state.email==="")||(this.state.pass===""))
{
alert('please fill all fields');
}
}
render() {
let fieldContainerClass = 'email-error'
const { email, valid } = this.state
if (!valid) {
fieldContainerClass += ' error'
}
return (
<div className="app flex-row align-items-center" id="forgot">
  <div className="container">
    <div className="row justify-content-center">
      <div className="col-md-6 cs wd-450">
        <div className="card mx-4">
          <div className="card-block p-4">
            <p className="text-muted">Enter Your Email
            </p>
            <div className="input-group mb-3">
              <span className="input-group-addon">@
              </span>
              <input type="text" className="form-control" placeholder="Email"
                     value={this.state.email}
                     onChange={this.handleEmailChange} 
                     onBlur={this.validateEmailBlur.bind(this)}
                     />
            </div>
            <span  className={fieldContainerClass}>Invalid e-mail address
            </span>
            <button type="button" className="btn btn-block btn-success"onClick={this.onSubmit.bind(this)}>Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
);
}
}
export default Forgot;
