import React, { Component } from 'react';
import {
    NavLink , Link
} from 'react-router-dom';
import firebaseApp from './Firebase.js';

class ManageProducts extends Component {
  constructor(props) {
    super(props);
 this.state = {


    };
    this.state.term="";
    
    this.state.products = [
     /* {
        id: 101,
        itemCode: 1025,
        Name: 'Green salad',
        category: 'Salad',
        size: '2.5L',
        price: '$12',
        
      }, {
       id: 102,
        itemCode: 1026,
        Name: 'Lemon Tea',
        category: 'beverages',
        size: '300 ML',
        price: '$15',
      }*/
       ];
       this.state.Category = [];
      this.state.value2="";
      this.state.name1="gourav";

  }


searchHandler(event)
{
this.setState(
  {term:event.target.value}
  )
}
onDelEvent(key){
  /*var id = prod;
  var index = -1;
  console.log(id)
  // this.state.products.splice(id, 1);
  // this.setState(this.state.products);

  for(var i = 0 ; i<this.state.products.length; i++)
  {
       if(this.state.products[i].id === id)
       {
          index = i;
          this.state.products.splice(index, 1);
          this.setState(this.state.products);
       }
  }  */ 
firebaseApp.database().ref().child('productList').child(key).remove();

// console.log(key)
}
changeinputcategory(event){
       this.setState({     
        value2:event.target.value
          })
    }

componentDidMount(){

this.fetchProductList("");

const dbRefObject1= firebaseApp.database().ref().child('productCategory').child('category');



     dbRefObject1.on('value', snap =>{
    var categories = [];
   
     snap.forEach((child) => {

      categories.push({
          category: child.val(),
          _key:child.val()
        });
     this.setState({
        Category:categories
                
})
     

});

  });

     


 /*  fetch('http://uat.nouns.in/api8', {
  method: 'post',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({"method":"getVendorBadges","service_key":"5ba470d11","userID":201,"searchValue":"","registrationType":16})
})
      .then((response) => response.json())
      .then((responseJson) => {
       this.setState({
        products:responseJson.result
      })
      })
      .catch((error) => {
        console.error(error);
      });
  }*/
 /* editdata(){

  data:this.state.products;
}
*/
}

 fetchProductList(category) {

  var dbRefObject= firebaseApp.database().ref().child('productList');
  dbRefObject = category == "" ? dbRefObject : dbRefObject.orderByChild("category").equalTo(category)

 /* dbRefObject.orderByChild("category").equalTo(category).on("child_added", function(snapshot) {
  var address = [];
    
     snapshot.forEach((child) => {

      address.push({
          Productid: child.val().Productid,
          Itemcode: child.val().Itemcode,
          pName: child.val().pName,
          category: child.val().category,
          size: child.val().size,
          price: child.val().price,
          category: child.val().category,
          _key: child.key
        });

 this.setState({
        products:address,
        rcount:address.length

        
})
      });
});*/

dbRefObject.on('value', snap =>{
    var address = [];
    
     snap.forEach((child) => {

      address.push({
          Productid: child.val().Productid,
          Itemcode: child.val().Itemcode,
          pName: child.val().pName,
          category: child.val().category,
          size: child.val().size,
          price: child.val().price,
          category: child.val().category,
          _key: child.key
        });

 this.setState({
        products:address,
        rcount:address.length

        
})
      });
  });
 }

 change(event){
  this.fetchProductList(event.target.value)
         //this.setState({value: event.target.value});

     }
     
     addCategory(){

firebaseApp.database().ref().child('productCategory').child('category').push(this.state.value2);

     }
 /*    sendValue(){
    browserHistory.push('/editproduct/' + this.state.products);
  }*/
  render() {
    return (
      <div className="manage-product">
      <div className="category-products-btn">
        <button type="button" className="btn btn-secondary" data-toggle="modal" data-target="#myModal2"><i className="icon-plus"></i>Add Category</button>
        <div id="myModal2" className="modal fade" role="dialog">
  <div className="modal-dialog">


    <div className="modal-content">
      <div className="modal-header">
        <h4 className="modal-title">Add Category</h4>
     <button type="button" className="close" data-dismiss="modal">&times;</button>

      </div>
      <div className="modal-body">
         <div className="form-group">
            <label htmlFor="recipient-name" className="control-label">Category Name:</label>
            <input type="text" className="form-control" id="recipient-name" onChange={this.changeinputcategory.bind(this)} value={this.state.value2}/>
          </div>
      </div>
      <div className="modal-footer">
        <NavLink to='/manageproducts'><button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.addCategory.bind(this)}>Save</button></NavLink>

        <button type="button" className="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>  
                <NavLink to='/addproduct'><button type="button" className="btn btn-secondary"><i className="icon-plus"></i>Add Products</button></NavLink>
        </div>

      <div className="animated fadeIn">
        <div className="row">

        

          <div className="col-lg-12">
            <div className="card">
              <div className="card-header">
                <i className="fa fa-align-justify"></i>Product List
              </div>
              <div className="form-group">
           <div className="col-sm-4">
          
           <select className="form-control" onChange={this.change.bind(this)} value={this.state.value}>
           <option value=""> Select Category</option>
            {
            this.state.Category.map(pcategory =>
           
           <option value={pcategory.category} key={pcategory._key}>{pcategory.category}</option>
           ) }
           </select>
        
           </div>
           <div className="col-sm-4"><input type="text" className="form-control" onChange={this.searchHandler.bind(this)} value={this.state.term} placeholder="Search by Name"/>
           </div>
           </div>
              <div className="card-block">
                <table className="table table-bordered table-striped table-sm">
                  <thead>
                    <tr>
                      <th>Product Id</th>
                      <th>Item Code</th>
                      <th>Name</th>
                      <th>category</th>
                      <th>Size</th>
                      <th>Price(Each)</th>
                      <th>Action</th>
                                       
                    </tr>
                  </thead>
                  <tbody>
                  {
        this.state.products.filter(searchingFor(this.state.term)).map(person =>
                    <tr key={person._key}>
                      <td>{person.Productid}</td>
                      <td>{person.Itemcode}</td>
                      <td>{person.pName}</td>
                      <td>{person.category}</td>
                      <td>{person.size}</td>
                      <td>{person.price}</td>
                      <td>
                      <span className="edit-product"><Link to= {{ pathname: '/editproduct', data:{person}}}><i className="icon-pencil"></i></Link></span>
                      <span onClick={this.onDelEvent.bind(this,person._key)} className="del-btn"><i className="icon-trash"></i></span></td>


                    </tr> 
                    )}                                  
                    </tbody>
                </table>
                <nav>
                  <ul className="pagination">
                    <li className="page-item"><a className="page-link" >Prev</a></li>
                    <li className="page-item active">
                      <a className="page-link" >1</a>
                    </li>
                    <li className="page-item"><a className="page-link" >2</a></li>
                    <li className="page-item"><a className="page-link" >3</a></li>
                    <li className="page-item"><a className="page-link" >4</a></li>
                    <li className="page-item"><a className="page-link" >Next</a></li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>

    )
  }
}

export default ManageProducts;

function searchingFor(term){
  return function(x){
  return x.pName.toLowerCase().includes(term.toLowerCase());

  }
}

